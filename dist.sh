#!/usr/bin/env sh
rm -R build 2>/dev/null
mkdir "build"
cd ./src/
npm install
cd ../
cp -R ./src/*.* ./build/
cp -R ./src/node_modules/ ./build/node_modules/
cp -R ./src/icons/ ./build/icons/
cd ./build/
zip -r ../build/build.zip ./
cd ../
rm -R dist 2>/dev/null
mkdir "dist"
cp ./build/build.zip ./dist/git-moji-chrome-X.XX.zip
mv ./build/build.zip ./dist/git-moji-firefox-X.XX.xpi

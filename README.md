# git-moji
Enhance your commits with emojis!

Available here [Firefox Add-ons](https://addons.mozilla.org/firefox/addon/git-moji/) | [Chrome Web Store](https://chrome.google.com/webstore/detail/mibjhinkhobaldjpkhlfmajehkibdbji).

![git-moji screenshot](git-moji-screenshot.png)

## Build
Simply clone the repo, ``cd`` in then run ``sh ./dist.sh``.
*Dependencies : you'll need the `zip` & `npm` command installed in your system.*